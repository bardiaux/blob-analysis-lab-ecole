![licence](https://img.shields.io/github/license/verdi8/blob-analysis-lab-demo)
# 🧫 Blob Analysis Lab Ecole
Cet outil a été initialement développé par [Laurent Knoll](https://github.com/verdi8) dans le cadre du projet de science participative [Derrière le blob, la recherche](https://www.cnrs.fr/fr/cnrsinfo/le-blob-et-la-demarche-scientifique) du CNRS. Les sources originales sur [GitHub](https://github.com/verdi8/blob-analysis-lab-demo).

Cette version est une ***adaptation*** de l'outil pour des séances en école élémentaire, avec la possibilité notamment d'utiliser des tablettes tactiles et d'obtenir facilement des mesures simples sur la croissance du blob (périmètre et aire). 

Lien vers la [page Github des séances](https://github.com/morganeTC/BlobEcoleElementaire/)

## Mod'Op
[Mode opératoire](docs/README.md)


## FAQ

## Licence
[MIT](LICENSE.md)
